<?php

namespace LowFrequency\ApiResponse;

use Illuminate\Support\ServiceProvider;
use LowFrequency\ApiResponse\APIResponse;

class ApiResponseServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
      //
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
      $this->app->make(APIResponse::class);
  }

  public function provides()
  {
    //
  }
}
