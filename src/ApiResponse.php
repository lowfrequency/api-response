<?php

namespace LowFrequency\ApiResponse;

use Carbon\Carbon;
use Illuminate\Support\Facades\URL;

class APIResponse
{

    public $_status_code;
    public $_links;
    public $_data;

    public function __construct()
    {
        $this->_status_code = 200;
        $this->_links = [];
        $this->_data = new \stdClass;
    }

    /**
     * Add a response code to API response
     *
     * @param int $code status code for response
     *
     */
    public function set_code($code = 200)
    {
        $this->_status_code = $code;
    }

    /**
     * Add a link info to API response
     *
     * @param string $rel The relationship between current doc and link
     * @param string $href The link to next doc
     * @param string $method The type of request GET, POST or DELETE
     *
     */
    public function add_link($rel = 'self', $href = 'v1/', $method = 'GET')
    {
        $url = URL::to('/') . $href;
        $this->_links[] = array('rel' => $rel, 'href' => $url, 'method' => $method);
    }


    /**
    * Add a data to API response data
    *
    * @param string $data The data to be added to response
    * @param string $type The name for the data to be called
    *
    * @return void
    *
    */
    public function add_data($type = '', $data = ''){
        $this->_data->$type = $data;
    }


    /**
    * Add a message to API response data
    *
    * @param string $message The message to be added to reponse
    * @param string $type The name for the data to be called
    *
    */
    public function add_message($message = ''){
        $this->add_data('message', $message);
    }


    /**
     * Add a denied message to API response data
     *
     * @param string $message The message to be added to reponse
     * @param string $code The HTTP code
     * @param string $status The status for the code response
     *
     */
    public function add_denied($message = 'Permission denied', $code = '403', $status = 'Forbidden')
    {
        $this->_status_code = $code;
        $this->_data->status = $status;
        $this->_data->message = $message;
    }


    /**
     * Add a OK message to API response data
     *
     * @param string $message The message to be added to reponse
     * @param string $code The HTTP code
     * @param string $status The status for the code response
     *
     */
    public function add_ok($message = 'HTTP_OK', $code = '200', $status = 'HTTP_OK')
    {
        $this->_status_code = $code;
        $this->_data->status = $status;
        $this->_data->message = $message;
    }

    /**
     *
     * Return response data
     *
     * @return array response data
     *
     */
    public function response()
    {
        $date_time = Carbon::now();
        $content = [
            'status'        => $this->_status_code,
            'completed_at'  => $date_time->toDateTimeString(),
            'links'         => $this->_links,
            'data'          => $this->_data,
        ];

        if($this->_status_code) {
            return response()->json($content, $this->_status_code);
        } else {
            return response()->json($content);
        }
    }
}