# Laravel API Response
### For Restful API's

Laravel does a great job formatting our responses into JSON and kicking them out appropriatly with application/json content types, but it does not help with any type of standard output format such as having a "status", or "messages" in the response. This class allows us to standardize all of our restful responses.


##### Install
````
$ composer require lowfrequency/api-response
````

##### Usage

This is intended to be used inside your controllers on a per method basis. Add the following to the top of your controller:
````
use LowFrequency\ApiResponse\APIResponse;
````

Example Method
````php
public function store(Request $request)
{
    $return = new APIResponse();

    // If something fails on execution, Maybe a query does not
    // return anything...
    $return->set_code(401);
    $return->addMessage("Model Not Found");


    // Add the payload. Can be any array or value and will return JSON
    $return->add_data( 'data', [
    	'foo'	=> 'bar',
        'bar'	=> true
    ]);
    $return->add_data( 'foo', 'bar');

    // Return the response.
    return $return->response();
}
````

Example Output:
````javascript
{
	"status": 401,
    "messages": [
    	"Model Not Found"
    ],
    "data": {
       	"foo": "bar",
    	[
        	"foo": "bar",
            "bar": true
        ]
    },
    "completed_at": "2019-06-06 12:06:33"
}
````
